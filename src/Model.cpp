#include "Model.h"

#include <GL/glew.h>

#define TINYOBJLOADER_IMPLEMENTATION
#include <tinyobjloader/tiny_obj_loader.h>

#include <vector>
#include <string>

#include <iostream>

Model::Model() :
	directory("resources/cube/"), file("cube.obj")
{
	LoadModel();
}

Model::Model(std::string model_path, std::string model_file, bool vertexOnly) :
	directory(model_path), file(model_file)
{
	if (vertexOnly)
	{
		LoadModelVertexOnly();
	}
	else
	{
		LoadModel();
	}
}

void Model::LoadModel()
{
	// Define obj file path
	const std::string obj_file = directory + file;
	
	// Setup tinyobj variables
	tinyobj::attrib_t attrib;
	std::vector<tinyobj::shape_t> shapes;
	std::vector<tinyobj::material_t> materials;

	std::string err;
	bool res = tinyobj::LoadObj(&attrib, &shapes, &materials, &err, 
		obj_file.c_str(), directory.c_str());

	// Report any errors or warning
	if (!err.empty())
	{
		std::cout << err << std::endl;
	}

	// Exit if loading failed
	if (res == false)
	{
		std::cout << "Error while loading " << obj_file << "!" << std::endl;
	}

	// Load all textures
	LoadTextures(materials);

	// Copy tinyobj values to own datastructures
	for (const auto& shape : shapes)
	{
		Mesh mesh;
		
		// Set diffuse color
		mesh.diffuse = GetDiffuseColor(shape, materials);

		// Reserve enough space to avoid reallocation
		mesh.positions.reserve(shape.mesh.indices.size());

		// Check if normals are specified
		if (attrib.normals.size() > 0)
		{
			mesh.has_normals = true;
			mesh.normals.reserve(shape.mesh.indices.size());
		}

		// Check if tex_coords are specified
		if (attrib.texcoords.size() > 0)
		{
			mesh.has_texcoords = true;
			mesh.tex_coords.reserve(shape.mesh.indices.size());
		}


		for (const auto& index : shape.mesh.indices)
		{

			// Read positions
			vec3 position = {
				attrib.vertices[3 * index.vertex_index + 0],
				attrib.vertices[3 * index.vertex_index + 1],
				attrib.vertices[3 * index.vertex_index + 2]
			};
			mesh.positions.push_back(position);

			// Read normals if available
			if (mesh.has_normals)
			{
				vec3 normal = {
					attrib.normals[3 * index.normal_index + 0],
					attrib.normals[3 * index.normal_index + 1],
					attrib.normals[3 * index.normal_index + 2]
				};
				mesh.normals.push_back(normal);
			}

			// Read texcoords if available
			if (mesh.has_texcoords)
			{
				// Flip tex_coords to fit opengl coordinate system
				vec2 tex_coord = {
					attrib.texcoords[2 * index.texcoord_index + 0],
					1.0f - attrib.texcoords[2 * index.texcoord_index + 1],
				};
				mesh.tex_coords.push_back(tex_coord);
			}

			mesh.indices.push_back(mesh.indices.size());
		}

		// Assign texture to mesh
		std::vector<std::string> paths;
		paths.emplace_back(directory + materials[shape.mesh.material_ids[0]].diffuse_texname);
		paths.emplace_back(directory + materials[shape.mesh.material_ids[0]].bump_texname);
		paths.emplace_back(directory + materials[shape.mesh.material_ids[0]].specular_texname);

		for (auto it = textures.begin(); it != textures.end(); ++it)
		{
			Texture* texture = &(*it);
			
			for (auto& tex_path : paths)
			{
				if (it->filename.compare(tex_path) == 0)
				{
					mesh.textures.push_back(texture);
				}
			}			
		}

		// Calculate tangents
		if (mesh.has_texcoords)
		{
			mesh.has_tangents = true;
			mesh.tangents.resize(mesh.indices.size());

			for (int i = 0; i < mesh.indices.size(); i += 3)
			{
				// Positions
				auto& p0 = mesh.positions[mesh.indices[i]];
				auto& p1 = mesh.positions[mesh.indices[i+1]];
				auto& p2 = mesh.positions[mesh.indices[i+2]];

				// TexCoords
				auto& tc0 = mesh.tex_coords[mesh.indices[i]];
				auto& tc1 = mesh.tex_coords[mesh.indices[i+1]];
				auto& tc2 = mesh.tex_coords[mesh.indices[i+2]];

				// Edges
				auto e1 = p1 - p0;
				auto e2 = p2 - p0;

				// Differences
				float delta_u1 = tc1.x - tc0.x;
				float delta_v1 = tc1.y - tc0.y;
				float delta_u2 = tc2.x - tc0.x;
				float delta_v2 = tc2.y - tc0.y;

				float f = 1.0f / (delta_u1 * delta_v2 - delta_u2 * delta_v1);

				if (isinf(f) == true)
				{
					f = 1;
				}

				// Tangent calculation
				vec3 tangent;

				tangent.x = f * (delta_v2 * e1.x - delta_v1 * e2.x);
				tangent.y = f * (delta_v2 * e1.y - delta_v1 * e2.y);
				tangent.z = f * (delta_v2 * e1.z - delta_v1 * e2.z);

				// No necessary?
				// Bitangent calculation
				//vec3 bitangent;

				mesh.tangents[mesh.indices[i]] += tangent;
				mesh.tangents[mesh.indices[i+1]] += tangent;
				mesh.tangents[mesh.indices[i+2]] += tangent;

			}

			// Normalize all tangents
			for (auto& tangent : mesh.tangents)
			{
				tangent = normalize(tangent);
			}
		}

		mesh.Init();

		meshes.push_back(mesh);
	}

}

void Model::LoadModelVertexOnly()
{
	// Define obj file path
	const std::string obj_file = directory + file;

	// Setup tinyobj variables
	tinyobj::attrib_t attrib;
	std::vector<tinyobj::shape_t> shapes;
	std::vector<tinyobj::material_t> materials;

	std::string err;
	bool res = tinyobj::LoadObj(&attrib, &shapes, &materials, &err,
		obj_file.c_str(), directory.c_str());

	// Report any errors or warning
	if (!err.empty())
	{
		std::cout << err << std::endl;
	}

	// Exit if loading failed
	if (res == false)
	{
		std::cout << "Error while loading " << obj_file << "!" << std::endl;
	}

	// Copy tinyobj values to own datastructures
	for (const auto& shape : shapes)
	{
		Mesh mesh;

		// Reserve enough space to avoid reallocation
		mesh.positions.reserve(shape.mesh.indices.size());

		// Check if normals are specified
		if (attrib.normals.size() > 0)
		{
			mesh.has_normals = true;
			mesh.normals.reserve(shape.mesh.indices.size());
		}

		mesh.has_texcoords = false;


		for (const auto& index : shape.mesh.indices)
		{
			// Read positions
			vec3 position = {
				attrib.vertices[3 * index.vertex_index + 0],
				attrib.vertices[3 * index.vertex_index + 1],
				attrib.vertices[3 * index.vertex_index + 2]
			};
			mesh.positions.push_back(position);

			// Read normals if available
			if (mesh.has_normals)
			{
				vec3 normal = {
					attrib.normals[3 * index.normal_index + 0],
					attrib.normals[3 * index.normal_index + 1],
					attrib.normals[3 * index.normal_index + 2]
				};
				mesh.normals.push_back(normal);
			}

			mesh.indices.push_back(mesh.indices.size());
		}

		//// Add simple texture
		//Texture* tex = new Texture(vec3(1.0, 0, 0));
		//mesh.textures.push_back(tex);

		mesh.Init();

		meshes.push_back(mesh);
	}

}

void Model::LoadTextures(const std::vector<tinyobj::material_t>& materials)
{
	// Load all textures
	for (auto& material : materials)
	{
		TextureType type;
		std::string tex_file;
		// Diffuse textures
		if (!material.diffuse_texname.empty())
		{
			type = TextureType::DIFFUSE;
			tex_file = directory + material.diffuse_texname;
			textures.emplace_back(tex_file, type);
		}

		// Normals textures
		if (!material.normal_texname.empty())
		{
			type = TextureType::NORMAL;
			tex_file = directory + material.normal_texname;
			textures.emplace_back(tex_file, type);
		}

		// Normals textures
		if (!material.bump_texname.empty())
		{
			type = TextureType::NORMAL;
			tex_file = directory + material.bump_texname;
			textures.emplace_back(tex_file, type);
		}

		// Specular textures
		if (!material.specular_texname.empty())
		{
			type = TextureType::SPECULAR;
			tex_file = directory + material.specular_texname;
			textures.emplace_back(tex_file, type);
		}

	}
}

vec3 Model::GetDiffuseColor(const tinyobj::shape_t& shape, 
	const std::vector<tinyobj::material_t>& materials)
{
	vec3 color;

	if (shape.mesh.material_ids.size() > 0)
	{
		const auto id = shape.mesh.material_ids[0];
		const auto& material = materials.at(id);
		const auto& diffuse = material.diffuse;

		color = vec3(diffuse[0], diffuse[1], diffuse[2]);
	}
	else
	{
		color = vec3(1.0f);
	}

	return color;
}

void Model::Render(Shader& shader)
{
	for (auto& mesh : meshes)
	{
		mesh.Render(shader);
	}
}
