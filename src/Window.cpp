#include "Window.h"

#include <iostream>

Window::Window() :
	Window(800, 600)
{
}

Window::Window(int width, int height) :
	Window(width, height, 250, 100)
{
}

Window::Window(int width, int height, int x, int y) :
	Window(width, height, x, y, "Advanced Comptuer Graphics")
{
}

Window::Window(int width, int height, int x, int y, std::string title) :
	width(width), height(height), x(x), y(y), title(title)
{
	Init();
}

void Window::Init()
{
	// Create window
	glWindow = glfwCreateWindow(width, height, title.c_str(),
		nullptr, nullptr);
	glfwMakeContextCurrent(glWindow);
	glfwSetWindowPos(glWindow, x, y);

	if (glWindow == nullptr)
	{
		std::cout << "Failed to create window" << std::endl;
		glfwTerminate();
		return;
	}

	// Set viewport size
	glfwGetFramebufferSize(glWindow, &width, &height);
	glViewport(0, 0, width, height);
}

float Window::GetAspectRatio()
{
	return static_cast<float>(width) / static_cast<float>(height);
}

