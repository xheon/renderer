#ifndef DEFERREDRENDERER_H
#define DEFERREDRENDERER_H

#include "Renderer.h"

#include "Scene.h"
#include "Window.h"

class DeferredRenderer : public Renderer
{
public:
	DeferredRenderer(Window* window, Scene* scene);
	~DeferredRenderer();

	// Inherited via Renderer
	virtual void Init() override;
	void InitSSAO();
	void InitAA();
	virtual void Render() override;

private:
	void InitGBuffers();
	void ShowGBuffers();

	void ShowSSAO();
	void ShowSSAOBlur();

	void GeometryPass();
	void LightingPass();
	
	void SSAOPass();
	void SSAOBlurPass();

	void AAPass();

	void RenderLights();
	void RenderQuad();

	void RenderUi();


// Variables
public:

	Scene* scene;
	Window* window;

private:
	// Deferred rendering
	GLuint gBuffer;
	GLuint gPosition, gNormal, gAlbedoSpec;
	GLuint rboDepth;

	// Shaders
	Shader* geometry_shader;
	Shader* lighting_shader;

	Shader* ssao_shader;
	Shader* ssao_blur_shader;

	Shader* fxaa_shader;

	Shader* lightBox_shader;

	// Rendering
	GLuint quadVAO = 0;
	GLuint quadTexCoord = 0;
	GLuint quadPosition = 0;

	// SSAO
	const int ssao_sample_count = 64;
	std::vector<vec3> ssaoSamples;
	GLuint noiseTexture;
	GLuint ssaoFBO;
	GLuint ssaoColorBuffer;

	// SSAO blur
	GLuint ssaoBlurFBO;
	GLuint ssaoBlurCBO;


	// Lighting
	GLuint lighting_bo;
	GLuint lighting_cb;

	// FXAA
	GLuint fxaa_bo;

	// UI
	bool showGBuffers;
	bool showSSAO;
	bool showSSAOBlur;
	bool applySSAO = true;
	float ssao_radius = 0.5f;
	float ssao_bias = 0.025f;
	float ssao_strength = 1.0f;
	int ssao_samples = 32;
	bool use_textures = true;
	int num_lights = 32;
	bool render_lights = false;
	bool use_normal_mapping = true;
	bool apply_fxaa = true;
};
#endif // !DEFERREDRENDERER_H


