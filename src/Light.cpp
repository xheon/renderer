#include "Light.h"

Light::Light() :
	ambient(1.0f), diffuse(1.0f), specular(1.0f)
{
}

Light::Light(vec3 ambient, vec3 diffuse, vec3 specular) :
	ambient(ambient), diffuse(diffuse), specular(specular)
{
}

void Light::Use(Shader & shader)
{
	shader.SetVec3("light.ambient", ambient);
	shader.SetVec3("light.diffuse", diffuse);
	shader.SetVec3("light.specular", specular);
}
