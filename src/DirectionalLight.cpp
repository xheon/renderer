#include "DirectionalLight.h"


DirectionalLight::DirectionalLight() :
	Light(), direction(vec3(-1.0f, -1.0f, -1.0f))
{
}

DirectionalLight::DirectionalLight(vec3 ambient, vec3 diffuse, vec3 specular, vec3 direction) :
	Light(ambient, diffuse, specular), direction(direction)
{
}

void DirectionalLight::Use(Shader & shader)
{
	shader.SetVec3("directionalLight.ambient", ambient);
	shader.SetVec3("directionalLight.diffuse", diffuse);
	shader.SetVec3("directionalLight.specular", specular);

	shader.SetVec3("directionalLight.direction", direction);
}
