#version 330 core

in vec2 position;
in vec2 uv;
in vec4 color;

uniform mat4 proj;

out vec2 frag_uv;
out vec4 frag_color;

void main()
{
	frag_uv = uv;
	frag_color = color;

	gl_Position = proj * vec4(position.xy, 0, 1);
}
