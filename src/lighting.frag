#version 330 core
out vec4 FragColor;

in vec2 TexCoords;

uniform sampler2D gPosition;
uniform sampler2D gNormal;
uniform sampler2D gAlbedoSpec;
uniform sampler2D gSSAO;

struct PointLight
{
	vec3 position;
	vec3 color;

	float constant;
	float linear;
	float quadratic;
};

const int MAX_pointLights = 512;
uniform int NUM_pointLights;
uniform PointLight pointLights[MAX_pointLights];

uniform bool has_ssao;



void main()
{             
    // retrieve data from gbuffer
    vec3 FragPos = texture(gPosition, TexCoords).rgb;
    vec3 Normal = texture(gNormal, TexCoords).rgb;
	Normal = normalize(Normal);

    vec3 Diffuse = texture(gAlbedoSpec, TexCoords).rgb;
    float Specular = texture(gAlbedoSpec, TexCoords).a;
	float occlusion = 1.0f;
	
	if(has_ssao)
	{
		occlusion = texture(gSSAO, TexCoords).r;
	}
    
    // then calculate lighting as usual
	vec3 ambient = Diffuse * 0.3 * occlusion;
    vec3 lighting = ambient;
	vec3 viewDir = normalize(-FragPos);
    
	for(int i = 0; i < NUM_pointLights; ++i)
    {	
		// diffuse
        vec3 lightDir = normalize(pointLights[i].position - FragPos);
        vec3 diffuse = max(dot(Normal, lightDir), 0.0) * Diffuse * pointLights[i].color;
        
		// specular
        vec3 halfwayDir = normalize(lightDir + viewDir);  
        float spec = pow(max(dot(Normal, halfwayDir), 0.0), 8.0);
        vec3 specular = pointLights[i].color * spec * Specular;
        
		// attenuation
        float dist = length(pointLights[i].position - FragPos);
        float attenuation = 1.0 / (1.0 + pointLights[i].linear * dist + pointLights[i].quadratic * dist * dist);
        
        lighting += (diffuse + specular) * attenuation;
		//lighting += specular * attenuation;
	}

    FragColor = vec4(lighting, 1.0);
    //FragColor = vec4(Specular, Specular, Specular, 1.0);
}