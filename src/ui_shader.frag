#version 330 core

in vec2 frag_uv;
in vec4 frag_color;

uniform sampler2D texture;

out vec4 out_color;

void main()
{
	out_color = frag_color * texture(texture, frag_uv.st);
}
