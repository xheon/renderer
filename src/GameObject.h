#ifndef GAMEOBJECT_H
#define GAMEOBJECT_H

#include <glm/glm.hpp>

#include "Model.h"
#include "Shader.h"

using namespace glm;

class GameObject
{
public:
	GameObject();
	GameObject(vec3 position);
	GameObject(Model model, vec3 position);

	Model model;
	vec3 position;
	vec3 size;
	vec3 rotation;

	void Render(Shader& shader, mat4 view_matrix, mat4 proj_matrix);
	void Render(Shader & shader);
};
#endif // !GAMEOBJECT_H


