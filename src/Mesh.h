#ifndef MESH_H
#define MESH_H

#include <vector>

#include <glm/glm.hpp>

#include "Texture.h"
#include "Shader.h"

using namespace glm;

// VERTEX
/*struct Vertex
{
	vec3 position;
	vec3 normal;
	vec2 tex_coords;
};*/


// MESH
class Mesh
{
public:
	std::vector<vec3> positions;
	std::vector<vec3> normals;
	std::vector<vec2> tex_coords;
	std::vector<vec3> tangents;
	std::vector<GLuint> indices;
	std::vector<Texture*> textures;
	vec3 diffuse;

	Mesh();

	void Init();

	void Render(Shader& shader);


	bool has_normals;
	bool has_texcoords;
	bool has_tangents;

private:
	GLuint vao_bo;

	GLuint positions_bo;
	GLuint normals_bo;
	GLuint texcoords_bo;
	GLuint tangents_bo;


};
#endif // !MESH_H


