#version 330 core

in vec2 fragTexCoord;

uniform sampler2D gPosition;
uniform sampler2D gNormal;
uniform sampler2D noise_texture;


const int MAX_samples = 64;
uniform int NUM_samples = 0;
uniform vec3 samples[MAX_samples];

uniform mat4 proj;

uniform vec2 noise_scale;

uniform float radius = 0.5f;
uniform float bias = 0.025f;
uniform float strength = 1.0f;

out float fragColor;

void main()
{
	// Get valeus from textures
	vec3 frag_position = texture(gPosition, fragTexCoord).rgb;
	vec3 frag_normal = texture(gNormal, fragTexCoord).rgb;
    frag_normal = normalize(frag_normal * 2.0 - 1.0); // Transform to [-1, 1]
	vec3 random_vec = normalize(texture(noise_texture, fragTexCoord * noise_scale).rgb);

	// Compute tanget space
	vec3 tangent = normalize(random_vec - frag_normal * dot(random_vec, frag_normal));
	vec3 bitangent = cross(frag_normal, tangent);
	mat3 TBN = mat3(tangent, bitangent, frag_normal);

	float occlusion = 0.0f;

	// Iterate over all samples
	for(int i = 0; i < NUM_samples; ++i)
	{
		// Convert sample into tangent space
		vec3 sample = TBN * samples[i];
		sample = frag_position + sample * radius; // Move sample along sample direction

		// Perspective projection
		vec4 offset = vec4(sample, 1.0f);
		offset = proj * offset;
		offset.xy /= offset.w;
		offset.xy = offset.xy * 0.5 + vec2(0.5); // Transform to [0, 1]

		// Sample position at calculated position
		float sample_depth = texture(gPosition, offset.xy).z; 

		// Range check
		float range_check = smoothstep(0.0, 1.0, radius / abs(frag_position.z - sample_depth));

		// Compare depths
		if(sample_depth >= sample.z + bias)
		{
			occlusion += range_check;
		}
	}

	// Normalize
	occlusion = 1.0 - occlusion / NUM_samples;
	fragColor = pow(occlusion, strength);
}