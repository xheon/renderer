#ifndef SHADER_H
#define SHADER_H

#include <string>
#include <fstream>
#include <unordered_map>

// GLEW
#include <GL/glew.h>

// GLFW
#include <GLFW/glfw3.h>

#include <glm/glm.hpp>

using namespace glm;

class Shader
{
public:
	Shader(const std::string& vertex_path, const std::string& fragment_path);

	void SetMat4(const std::string& uniform, mat4 matrix);
	void SetVec2(const std::string& uniform, vec2 value);
	void SetVec3(const std::string& uniform, vec3 value);
	void SetFloat(const std::string& uniform, float value);
	void SetInt(const std::string& uniform, int value);
	void SetBool(const std::string & uniform, bool value);

	void EnableAttribute(const std::string& attrib);
	void SetAttribute(const std::string& attrib, int size, GLenum type, bool normalized, int stride,
		void* pointer);

	GLuint GetAttributeLocation(const std::string & uniform);

	GLuint GetUniformLocation(const std::string & uniform);

	//int AddAttribute(const std::string& attribute);

	void Use();

	GLuint program;

	const std::string vertexShader_file;
	const std::string fragmentShader_file;

	//std::unordered_map<const std::string, int> attributes;
};

#endif /* SHADER_H */