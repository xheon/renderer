#version 330 core

// Type definitions
struct Material
{
	sampler2D diffuse;
	sampler2D specular;
	float shininess;
};

struct PointLight
{
	vec3 position;

	vec3 ambient;
	vec3 diffuse;
	vec3 specular;

	// Attenuation
	float constant;
	float linear;
	float quadratic;
};

struct DirectionalLight
{
	vec3 direction;

	vec3 ambient;
	vec3 diffuse;
	vec3 specular;
};


// In-variables
in vec3 fragPos;
in vec2 outTexCoord;
in vec3 outNormal;


// Uniforms
uniform vec3 viewPos;
uniform Material material;

// Lights
uniform DirectionalLight directionalLight;

#define NUM_POINTLIGHTS 5
uniform PointLight pointLights[NUM_POINTLIGHTS];


// Out-variables
out vec4 color;

// Function prototypes
vec3 CalcDirectionalLightContribution(DirectionalLight light, vec3 normal, vec3 viewDir);
vec3 CalcPointLightContribution(PointLight light, vec3 normal, vec3 fragPos, vec3 viewDir);

void main()
{
	vec3 normal = normalize(outNormal);
	vec3 viewDir = normalize(viewPos - fragPos);
	
	vec3 result = CalcDirectionalLightContribution(directionalLight, normal, viewDir);

	for(int i = 0; i < NUM_POINTLIGHTS; i++)
	{
		result += CalcPointLightContribution(pointLights[i], normal, fragPos, viewDir);
	}

	color = vec4(result, 1.0f);
}

vec3 CalcDirectionalLightContribution(DirectionalLight light, vec3 normal, vec3 viewDir)
{
	// Diffuse	
	vec3 lightDir = normalize(-light.direction);
	float diffuse_factor = max(dot(normal, lightDir), 0.0f);

	// Specular
	vec3 reflectDir = reflect(-lightDir, normal);
	float specular_factor = pow(max(dot(viewDir, reflectDir), 0.0f), material.shininess);
	
	// Phong parts
	vec3 ambient = light.ambient * vec3(texture(material.diffuse, outTexCoord));
	vec3 diffuse = light.diffuse * diffuse_factor * vec3(texture(material.diffuse, outTexCoord));
	vec3 specular = light.specular * specular_factor * vec3(texture(material.specular, outTexCoord));

	return vec3(ambient + diffuse + specular);
}

vec3 CalcPointLightContribution(PointLight light, vec3 normal, vec3 fragPos, vec3 viewDir)
{
	// Diffuse	
	vec3 lightDir = normalize(light.position - fragPos);
	float diffuse_factor = max(dot(normal, lightDir), 0.0f);

	// Specular
	vec3 reflectDir = reflect(-lightDir, normal);
	float specular_factor = pow(max(dot(viewDir, reflectDir), 0.0f), material.shininess);
	
	// Phong parts
	vec3 ambient = light.ambient * vec3(texture(material.diffuse, outTexCoord));
	vec3 diffuse = light.diffuse * diffuse_factor * vec3(texture(material.diffuse, outTexCoord));
	vec3 specular = light.specular * specular_factor * vec3(texture(material.specular, outTexCoord));

	// Attenuation
	float dist = length(light.position - fragPos);
	float factor = light.constant + light.linear * dist + light.quadratic * (dist * dist);
	float attenuation = 1.0f / factor;
	vec3 contribution = ambient + diffuse + specular;

	contribution *= attenuation;

	return contribution;
}
