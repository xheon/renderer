#include "Engine.h"

#include <imgui/imgui.h>
#include "ImGui_impl.h"

#include <iostream>

#include "Shader.h"
#include "DeferredRenderer.h"


Engine::Engine()
{
}

Engine::~Engine()
{
	delete renderer;
	delete input;
	delete window;
	//delete ui;
	ImGui_Shutdown();

}

void Engine::Init()
{
	InitGL();
	window = new Window(1280, 650, 50, 50, "ACG - Manuel Dahnert");
	InitGLEW();
	input = new Input(window);
	//ui = new UI();
	ImGui_Init(window->glWindow, false);

	// Define opengl states
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);


	scene.Init();
	renderer = new DeferredRenderer(window, &scene);
	renderer->Init();
}

void Engine::InitGL()
{
	// Initialize glfw
	glfwInit();
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);
}

void Engine::InitGLEW()
{
	// Initialize GLEW
	glewExperimental = GL_TRUE;

	if (glewInit() != GLEW_OK)
	{
		std::cout << "Failed to initialize GLEW" << std::endl;
		return;
	}
}


void Engine::Update()
{
	glfwPollEvents();
	ImGui_NewFrame();

	GLfloat currentFrame = glfwGetTime();
	deltaTime = currentFrame - lastFrame;
	lastFrame = currentFrame;

	// Check and process events
	input->ProcessInput(deltaTime, &(scene.camera));

	// Update scene
	scene.Update(deltaTime);

	// Render scene
	renderer->Render();

	// Render UI
	ImGui::Render();


	// Swap buffers
	glfwSwapBuffers(window->glWindow);
}

bool Engine::IsRunning()
{
	isRunning = !glfwWindowShouldClose(window->glWindow);
	
	return isRunning;
}
