#ifndef POINTLIGHT_H
#define POINTLIGHT_H

#include "Light.h"

#include "Model.h"

class PointLight : public Light
{
public:
	PointLight(vec3 ambient, vec3 diffuse, vec3 specular, vec3 position,
		float constant, float linear, float quadratic, int index);

	vec3 position;
	float constant;
	float linear;
	float quadratic;

	int index;

	void Use(Shader& shader) override;
	void Use(Shader& shader, const mat4& view);
};

#endif // !POINTLIGHT_H
