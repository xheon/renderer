#include "Mesh.h"

Mesh::Mesh() :
	has_normals(false), has_texcoords(false), has_tangents(false)
{
}

void Mesh::Init()
{
	// Create buffer objects
	glGenVertexArrays(1, &vao_bo);
	glBindVertexArray(vao_bo);

	// Vertex positions
	glGenBuffers(1, &positions_bo);
	glBindBuffer(GL_ARRAY_BUFFER, positions_bo);
	glBufferData(GL_ARRAY_BUFFER, positions.size() * sizeof(vec3), 
		&positions[0].x, GL_STATIC_DRAW);
	
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), 0);
	glEnableVertexAttribArray(0);

	// Vertex normals
	if (has_normals == true)
	{
		glGenBuffers(1, &normals_bo);
		glBindBuffer(GL_ARRAY_BUFFER, normals_bo);
		glBufferData(GL_ARRAY_BUFFER, normals.size() * sizeof(vec3),
			&normals[0].x, GL_STATIC_DRAW);

		glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), 0);
		glEnableVertexAttribArray(1);
	}

	// Vertex tex_coords
	if (has_texcoords == true)
	{
		glGenBuffers(1, &texcoords_bo);
		glBindBuffer(GL_ARRAY_BUFFER, texcoords_bo);
		glBufferData(GL_ARRAY_BUFFER, tex_coords.size() * sizeof(vec2),
			&tex_coords[0].x, GL_STATIC_DRAW);

		glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 2 * sizeof(float), 0);
		glEnableVertexAttribArray(2);
	}

	// Tangents
	if (has_tangents == true)
	{
		glGenBuffers(1, &tangents_bo);
		glBindBuffer(GL_ARRAY_BUFFER, tangents_bo);
		glBufferData(GL_ARRAY_BUFFER, tangents.size() * sizeof(vec3),
			&tangents[0].x, GL_STATIC_DRAW);

		glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), 0);
		glEnableVertexAttribArray(3);
	}

	// Disable current mesh after initialization
	glBindVertexArray(0);
}

void Mesh::Render(Shader& shader)
{
	std::vector<bool> bits(3, false);
	
	// Bind textures
	for (auto& texture : textures)
	{
		bool bind = true;

		switch (texture->type)
		{
		case TextureType::DIFFUSE:
			glActiveTexture(GL_TEXTURE0);
			shader.SetInt("texture_diffuse", 0);
			bits[0] = true;
			break;
	
		case TextureType::SPECULAR:
			glActiveTexture(GL_TEXTURE1);
			shader.SetInt("texture_specular", 1);
			bits[1] = true;
			break;

		case TextureType::NORMAL:
			glActiveTexture(GL_TEXTURE2);
			shader.SetInt("texture_normal", 2);
			bits[2] = true;
			break;

		default:
			bind = false;
			break;
		}


		// Only bind if a texture unit is active
		if (bind) {
			glBindTexture(GL_TEXTURE_2D, texture->texture);
		}

	}

	shader.SetBool("has_specular", bits[1]);

	// Render mesh
	glBindVertexArray(vao_bo);
	glDrawArrays(GL_TRIANGLES, 0, indices.size());
	glBindVertexArray(0);
}
