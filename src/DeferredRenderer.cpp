#include "DeferredRenderer.h"

#include <iostream>
#include <random>
#include <algorithm>

#include <imgui/imgui.h>

#include "Engine.h"
#include "SimpleMath.h"

DeferredRenderer::DeferredRenderer(Window* window, Scene* scene) :
	window(window), scene(scene)
{
}


DeferredRenderer::~DeferredRenderer()
{
	delete geometry_shader;
	delete lighting_shader;
}

void DeferredRenderer::Init()
{
	InitGBuffers();
	
	geometry_shader = new Shader("src/geometry.vert", "src/geometry.frag");
	lighting_shader = new Shader("src/lighting.vert", "src/lighting.frag");
	lightBox_shader = new Shader("src/lamp.vert", "src/lamp.frag");

	ssao_shader = new Shader("src/ssao_shader.vert", "src/ssao_shader.frag");
	ssao_blur_shader = new Shader("src/ssao_shader.vert", "src/ssao_blur.frag");

	fxaa_shader = new Shader("src/lighting.vert", "src/fxaa.frag");

	InitSSAO();
	InitAA();
}

void DeferredRenderer::InitSSAO()
{
	// Define SSAO random samples
	std::uniform_real_distribution<float> random_floats(0.0f, 1.0f);
	std::default_random_engine generator;

	for (int i = 0; i < ssao_sample_count; i++)
	{
		// X and Y: [-1, 1]
		// Z: [0, 1]
		// Due to tanget space where Z points to +z
		vec3 sample(
			random_floats(generator) * 2.0f - 1.0f,
			random_floats(generator) * 2.0f - 1.0f,
			random_floats(generator)
		);

		sample = normalize(sample);
		sample *= random_floats(generator);

		float scale = static_cast<float>(i) / ssao_sample_count;
		scale = Math::Lerp(0.1f, 1.0f, scale * scale);
		sample *= scale;
		ssaoSamples.push_back(sample);
	}

	// 4x4 noise texture
	std::vector<vec3> ssaoNoise;
	for (int i = 0; i < 16; i++)
	{
		vec3 noise(
			random_floats(generator) * 2.0f - 1.0f,
			random_floats(generator) * 2.0f - 1.0f,
			0.0f
		);

		ssaoNoise.push_back(noise);
	}

	glGenTextures(1, &noiseTexture);
	glBindTexture(GL_TEXTURE_2D, noiseTexture);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB32F, 4, 4, 0, GL_RGB, GL_FLOAT, &ssaoNoise[0]);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

	glGenFramebuffers(1, &ssaoFBO);
	glBindFramebuffer(GL_FRAMEBUFFER, ssaoFBO);

	glGenTextures(1, &ssaoColorBuffer);
	glBindTexture(GL_TEXTURE_2D, ssaoColorBuffer);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RED, window->width, window->height, 0, GL_RGB,
		GL_FLOAT, nullptr);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D,
		ssaoColorBuffer, 0);

	// SSAO blur
	glGenFramebuffers(1, &ssaoBlurFBO);
	glBindFramebuffer(GL_FRAMEBUFFER, ssaoBlurFBO);

	glGenTextures(1, &ssaoBlurCBO);
	glBindTexture(GL_TEXTURE_2D, ssaoBlurCBO);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RED, window->width, window->height, 0,
		GL_RGB, GL_FLOAT, nullptr);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D,
		ssaoBlurCBO, 0);

	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void DeferredRenderer::InitAA()
{
	glGenFramebuffers(1, &fxaa_bo);
	glBindFramebuffer(GL_FRAMEBUFFER, fxaa_bo);

	/*glGenTextures(1, &fxaa_cb);
	glBindTexture(GL_TEXTURE_2D, fxaa_cb);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB32F, window->width, window->height, 0,
		GL_RGB, GL_FLOAT, nullptr);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D,
		fxaa_cb, 0);*/

	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void DeferredRenderer::Render()
{
	glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	
	GeometryPass();

	if (showGBuffers)
	{
		ShowGBuffers();
	}
	else
	{
		if (applySSAO)
		{
			// SSAO
			SSAOPass();
			SSAOBlurPass();
		}

		if (applySSAO && showSSAO)
		{
			if (showSSAOBlur)
			{
				ShowSSAOBlur();
			}
			else
			{
				ShowSSAO();
			}
		}
		else
		{
			LightingPass();

			// Post-Processing
			if (apply_fxaa)
			{
				AAPass();
			}


			// Render lights with forward rendering
			if (render_lights)
			{
				RenderLights();
			}
		}
	}

	RenderUi();
}



void DeferredRenderer::InitGBuffers()
{
	// Init deferred rendering buffers
	glGenFramebuffers(1, &gBuffer);
	glBindFramebuffer(GL_FRAMEBUFFER, gBuffer);

	// Position buffer
	glGenTextures(1, &gPosition);
	glBindTexture(GL_TEXTURE_2D, gPosition);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB16F, window->width, window->height, 0, GL_RGB,
		GL_FLOAT, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D,
		gPosition, 0);

	// Normal buffer
	glGenTextures(1, &gNormal);
	glBindTexture(GL_TEXTURE_2D, gNormal);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB16F, window->width, window->height, 0, GL_RGB,
		GL_FLOAT, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, GL_TEXTURE_2D,
		gNormal, 0);

	// Color buffer
	glGenTextures(1, &gAlbedoSpec);
	glBindTexture(GL_TEXTURE_2D, gAlbedoSpec);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, window->width, window->height, 0, GL_RGBA,
		GL_UNSIGNED_BYTE, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT2, GL_TEXTURE_2D,
		gAlbedoSpec, 0);

	GLenum gAttachments[] = { GL_COLOR_ATTACHMENT0 , GL_COLOR_ATTACHMENT1 , GL_COLOR_ATTACHMENT2 };

	glDrawBuffers(3, gAttachments);

	// Define Depth buffer
	glGenRenderbuffers(1, &rboDepth);
	glBindRenderbuffer(GL_RENDERBUFFER, rboDepth);
	glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, window->width, window->height);
	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, rboDepth);

	// finally check if framebuffer is complete
	GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);

	if (status != GL_FRAMEBUFFER_COMPLETE) {
		std::cout << "Framebuffer error, status: " << std::hex << status << std::endl;
		return;
	}

	// Lighting
	glGenFramebuffers(1, &lighting_bo);
	glBindFramebuffer(GL_FRAMEBUFFER, lighting_bo);

	glGenTextures(1, &lighting_cb);
	glBindTexture(GL_TEXTURE_2D, lighting_cb);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB16F, window->width, window->height, 0,
	GL_RGB, GL_FLOAT, nullptr);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D,
		lighting_cb, 0);

	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void DeferredRenderer::ShowGBuffers()
{
	// Show gbuffers
	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glBindFramebuffer(GL_READ_FRAMEBUFFER, gBuffer);

	int half_width = window->width / 2;
	int half_height = window->height / 2;

	// Position
	glReadBuffer(GL_COLOR_ATTACHMENT0);
	glBlitFramebuffer(0, 0, window->width, window->height,
		0, half_height, half_width, window->height,
		GL_COLOR_BUFFER_BIT, GL_LINEAR);

	// Normal
	glReadBuffer(GL_COLOR_ATTACHMENT1);
	glBlitFramebuffer(0, 0, window->width, window->height,
		half_width, half_height, window->width, window->height,
		GL_COLOR_BUFFER_BIT, GL_LINEAR);

	// Diffuse
	glReadBuffer(GL_COLOR_ATTACHMENT2);
	glBlitFramebuffer(0, 0, window->width, window->height,
		0, 0, half_width, half_height,
		GL_COLOR_BUFFER_BIT, GL_LINEAR);

	// Specular
	/*glReadBuffer(GL_COLOR_ATTACHMENT3);
	glBlitFramebuffer(0, 0, 800, 600, 400, 300, 800, 600, GL_COLOR_BUFFER_BIT, GL_LINEAR);*/
}

void DeferredRenderer::ShowSSAO()
{
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glBindFramebuffer(GL_READ_FRAMEBUFFER, ssaoFBO);
	glReadBuffer(GL_COLOR_ATTACHMENT0);
	glBlitFramebuffer(0, 0, window->width, window->height, 0, 0, window->width,
		window->height, GL_COLOR_BUFFER_BIT, GL_LINEAR);

}

void DeferredRenderer::ShowSSAOBlur()
{
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glBindFramebuffer(GL_READ_FRAMEBUFFER, ssaoBlurFBO);
	glReadBuffer(GL_COLOR_ATTACHMENT0);
	glBlitFramebuffer(0, 0, window->width, window->height, 0, 0, window->width,
		window->height, GL_COLOR_BUFFER_BIT, GL_LINEAR);

}

void DeferredRenderer::GeometryPass()
{
	mat4 proj = perspective(radians(45.0f), window->GetAspectRatio(), 0.1f, 1000.0f);
	mat4 view = scene->camera.GetViewMatrix();

	// Geometry pass
	glBindFramebuffer(GL_FRAMEBUFFER, gBuffer);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	geometry_shader->Use();
	geometry_shader->SetMat4("proj", proj);
	geometry_shader->SetMat4("view", view);

	geometry_shader->SetBool("use_textures", use_textures);
	geometry_shader->SetBool("use_normal_mapping", use_normal_mapping);

	for (auto& object : scene->GetObjects())
	{
		object.Render(*geometry_shader);
	}

	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void DeferredRenderer::LightingPass()
{
	// Draw full screen quad using the lighting shader

	if (apply_fxaa)
	{
		glBindFramebuffer(GL_FRAMEBUFFER, lighting_bo);
	}
	else
	{
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
	}

	// Lighting pass
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	lighting_shader->Use();
	//lighting_shader->SetVec3("viewPos", scene->camera.position);

	auto max_lights = std::min(num_lights,
		static_cast<int>(scene->GetLights().size()));
	lighting_shader->SetInt("NUM_pointLights", max_lights);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, gPosition);
	lighting_shader->SetInt("gPosition", 0);

	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, gNormal);
	lighting_shader->SetInt("gNormal", 1);

	glActiveTexture(GL_TEXTURE2);
	glBindTexture(GL_TEXTURE_2D, gAlbedoSpec);
	lighting_shader->SetInt("gAlbedoSpec", 2);

	glActiveTexture(GL_TEXTURE3);
	glBindTexture(GL_TEXTURE_2D, ssaoBlurCBO);
	lighting_shader->SetInt("gSSAO", 3);

	lighting_shader->SetBool("has_ssao", applySSAO);

	//for (auto& pl : scene->GetLights())
	for(int i = 0; i < max_lights; ++i)
	{
		auto& pl = scene->GetLights().at(i);
		//pl.position = vec3( * vec4(pl.position, 1.0));
		pl.Use(*lighting_shader, scene->camera.GetViewMatrix());
	}

	// Render quad
	RenderQuad();

	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void DeferredRenderer::SSAOPass()
{
	glBindFramebuffer(GL_FRAMEBUFFER, ssaoFBO);
	glClear(GL_COLOR_BUFFER_BIT);
	ssao_shader->Use();

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, gPosition);
	
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, gNormal);

	glActiveTexture(GL_TEXTURE2);
	glBindTexture(GL_TEXTURE_2D, noiseTexture);

	mat4 proj = perspective(radians(45.0f), window->GetAspectRatio(), 0.1f, 1000.0f);
	ssao_shader->SetMat4("proj", proj);
	ssao_shader->SetVec2("noise_scale", vec2(window->width/4.0f, window->height/4.0f));
	ssao_shader->SetInt("gPosition", 0);
	ssao_shader->SetInt("gNormal", 1);
	ssao_shader->SetInt("noise_texture", 2);

	ssao_shader->SetFloat("radius", ssao_radius);
	ssao_shader->SetFloat("bias", ssao_bias);
	ssao_shader->SetFloat("strength", ssao_strength);

	ssao_shader->SetInt("NUM_samples", ssao_samples);

	// Send samples to shader
	for (int i = 0; i < ssao_samples; i++)
	{
		ssao_shader->SetVec3("samples[" + std::to_string(i) + "]", ssaoSamples[i]);
	}

	RenderQuad();

	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void DeferredRenderer::SSAOBlurPass()
{
	glBindFramebuffer(GL_FRAMEBUFFER, ssaoBlurFBO);
	glClear(GL_COLOR_BUFFER_BIT);

	ssao_blur_shader->Use();
	ssao_blur_shader->SetInt("ssaoInput", 0);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, ssaoColorBuffer);

	RenderQuad();

	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void DeferredRenderer::AAPass()
{
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glClear(GL_COLOR_BUFFER_BIT);

	fxaa_shader->Use();
	fxaa_shader->SetInt("filter_texture", 0);
	fxaa_shader->SetVec2("texture_size", vec2(window->width, window->height));

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, lighting_cb);

	RenderQuad();

	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void DeferredRenderer::RenderLights()
{
	// Copy saved depth buffer back to default framebuffer
	glBindFramebuffer(GL_READ_FRAMEBUFFER, rboDepth);
	glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
	glBlitFramebuffer(0, 0, window->width, window->height,
		0, 0, window->width, window->height, GL_DEPTH_BUFFER_BIT,
		GL_NEAREST);

	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	// Configure shader
	mat4 proj = perspective(radians(45.0f), window->GetAspectRatio(), 0.1f, 1000.0f);

	lightBox_shader->Use();
	lightBox_shader->SetMat4("view", scene->camera.GetViewMatrix());
	lightBox_shader->SetMat4("proj", proj);

	auto max_lights = std::min(num_lights,
		static_cast<int>(scene->GetLightObjects().size()));
	for (int i = 0; i < max_lights; ++i)
	{
		auto& pl = scene->GetLightObjects().at(i);
		lightBox_shader->SetVec3("lightColor", pl.model.meshes[0].diffuse);
		pl.Render(*lightBox_shader);
	}


}

void DeferredRenderer::RenderQuad()
{
	if (quadVAO == 0)
	{
		GLfloat quadVertices[] = {
			// Positions
			-1.0f,	 1.0f,	0.0f,
			-1.0f,	-1.0f,	0.0f,
			1.0f,	 1.0f,	0.0f,
			1.0f,	-1.0f,	0.0f
		};

		GLfloat quadTC[] = {
			// Texture Coords
			0.0f, 1.0f,
			0.0f, 0.0f,
			1.0f, 1.0f,
			1.0f, 0.0f
		};

		// Setup plane VAO
		glGenVertexArrays(1, &quadVAO);
		glBindVertexArray(quadVAO);

		glGenBuffers(1, &quadPosition);
		glBindBuffer(GL_ARRAY_BUFFER, quadPosition);
		glBufferData(GL_ARRAY_BUFFER, sizeof(quadVertices), &quadVertices, GL_STATIC_DRAW);

		glEnableVertexAttribArray(0);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), 0);

		glGenBuffers(1, &quadTexCoord);
		glBindBuffer(GL_ARRAY_BUFFER, quadTexCoord);
		glBufferData(GL_ARRAY_BUFFER, sizeof(quadTC), &quadTC, GL_STATIC_DRAW);

		glEnableVertexAttribArray(1);
		glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 2 * sizeof(GLfloat), 0);
	}

	glBindVertexArray(quadVAO);
	glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
	glBindVertexArray(0);
}

void DeferredRenderer::RenderUi()
{
	{
		ImGui::Begin("Settings");
		ImGui::Checkbox("Update Scene", &scene->update_scene);
		ImGui::SliderFloat("Velocity", &scene->velocity, 2, 50);

		ImGui::Separator();

		
		ImGui::Checkbox("Textures", &use_textures);
		ImGui::Checkbox("Normal mapping", &use_normal_mapping);
		ImGui::Checkbox("FXAA", &apply_fxaa);

		ImGui::SliderInt("#Lights", &num_lights, 1, scene->GetLights().size());
		ImGui::Checkbox("Render lights", &render_lights);
		ImGui::Checkbox("Show G-Buffers", &showGBuffers);

		ImGui::Separator();

		ImGui::Checkbox("Show SSAO-pass", &showSSAO);
		ImGui::Checkbox("Show SSAO blur", &showSSAOBlur);
		ImGui::Checkbox("Apply SSAO", &applySSAO);

		ImGui::SliderInt("Samples", &ssao_samples, 1, ssao_sample_count);
		ImGui::SliderFloat("SSAO radius", &ssao_radius, 0.1f, 5.0f);
		ImGui::SliderFloat("SSAO bias", &ssao_bias, 0.0f, 1.0f);
		ImGui::SliderFloat("SSAO strength", &ssao_strength, 0.0f, 5.0f);
		ImGui::End();
	}
}
