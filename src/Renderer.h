#ifndef RENDERER_H
#define RENDERER_H

class Renderer
{
public:
	virtual ~Renderer() {};

	virtual void Init() = 0;
	virtual void Render() = 0;
};

#endif // !RENDERER_H
