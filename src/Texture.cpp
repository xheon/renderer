#include <assert.h>
#include "Texture.h"

#include <array>
#include <algorithm>

#define STB_IMAGE_IMPLEMENTATION
#include <stb/stb_image.h>

Texture::Texture(std::string & filename, TextureType type) :
	type(type)
{
	this->filename = NormalizePath(filename);

	LoadTexture();
}

Texture::Texture(vec3 color, TextureType type) :
	type(type)
{
	color *= 255;
	std::array<GLubyte, 4> data = { 
		static_cast<GLubyte>(color.x), 
		static_cast<GLubyte>(color.y), 
		static_cast<GLubyte>(color.z), 255 };
	
	glGenTextures(1, &texture);
	glBindTexture(GL_TEXTURE_2D, texture);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, 1, 1, 0, GL_RGBA, GL_UNSIGNED_BYTE,
		data.data());

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);	// Wraping u
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);	// Wrapping v

	glGenerateMipmap(GL_TEXTURE_2D);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);	// Filter mag
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);	// Filter min
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY_EXT, 16.0f);

	glBindTexture(GL_TEXTURE_2D, 0);
}

void Texture::LoadTexture()
{
	// Load the image using STB
	int comp;
	unsigned char* image = stbi_load(filename.c_str(), &width, &height, &comp, STBI_rgb_alpha);

	// Throw an exception if image could not be loaded
	assert(image != nullptr);
	/*if(image == nullptr)
	{
		throw std::runtime_error("LoadTexture: Failed to load texture image");
	}*/

	// Generate texture
	glGenTextures(1, &texture);
	glBindTexture(GL_TEXTURE_2D, texture);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, image);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);	// Wraping u
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);	// Wrapping v

	glGenerateMipmap(GL_TEXTURE_2D);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);	// Filter mag
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);	// Filter min
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY_EXT, 16.0f);
	
	// Clean up
	stbi_image_free(image);

	// Unbind current texture
	glBindTexture(GL_TEXTURE_2D, 0);
}

std::string Texture::NormalizePath(std::string& path)
{
	std::replace(path.begin(), path.end(), '\\', '/');

	return path;
}
