#ifndef SCENE_H
#define SCENE_H

#include <vector>

#include "GameObject.h"
#include "PointLight.h"
#include "Camera.h"

class Scene
{
public:
	Scene();

	void Init();

	std::vector<GameObject>& GetObjects();
	std::vector<PointLight>& GetLights();
	std::vector<GameObject>& GetLightObjects();


	void Update(float delta);

	Camera camera;

	bool update_scene = true;
	float velocity = 5.0f;

	bool first_update = true;

private:
	void InitObjects();
	void InitLights();

	std::vector<GameObject> objects;
	std::vector<PointLight> pointLights;
	std::vector<GameObject> debugPointLights;
};

#endif // !SCENCE_H

