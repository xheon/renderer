#ifndef ENGINE_H
#define ENGINE_H

// GLEW
#include <GL/glew.h>

// GLFW
#include <GLFW\glfw3.h>

// GLM
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

// STD
#include <unordered_map>

// Own includes
#include "Camera.h"
#include "Model.h"
#include "GameObject.h"
#include "PointLight.h"
#include "Scene.h"
#include "Window.h"
#include "Renderer.h"
#include "Input.h"
//#include "UI.h"

class Engine
{
public:
	Engine();
	~Engine();

	void Init();
	void Update();
	bool IsRunning();

private:
	void InitGL();
	void InitGLEW();


private:
	bool isRunning;

	Window* window;
	Input* input;
	//UI* ui;

	Renderer* renderer;

	// Framerate independence
	GLfloat deltaTime = 0.0f;
	GLfloat lastFrame = 0.0f;

	// Scene
	Scene scene;
};


#endif // !ENGINE_H

