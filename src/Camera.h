#ifndef CAMERA_H
#define CAMERA_H

#include <glm/glm.hpp>

#include "Key.h"

using namespace glm;

// Default values
const float YAW = -90.0f;
const float PITCH = 0.0f;
const float SPEED = 12.0f;
const float SENSITIVITY = 0.25f;


class Camera
{
public:
	Camera(vec3 position = vec3( 0.0f, 0.0f, 0.0f ), 
		vec3 up = vec3(0.0f, 1.0f, 0.0f), float yaw = YAW, float pitch = PITCH);

	mat4 GetViewMatrix();

//private:
	vec3 position;
	vec3 up;
	vec3 front;
	vec3 right;

	vec3 worldUp;

	float yaw;
	float pitch;

	float speed;
	float sensitivity;

	void Update();

	void ProcessKeyboard(Key direction, float delta);
	void ProcessMouse(float xoffset, float yoffset);


};
#endif // !CAMERA_H


