#version 330 core

layout (location = 0) out vec3 gPosition;
layout (location = 1) out vec3 gNormal;
layout (location = 2) out vec4 gColorSpec;

in vec3 fragPosition;
in vec3 fragNormal;
in vec2 fragTexCoord;
in vec3 fragColor;
in vec3 fragTangent;

uniform bool use_textures;
uniform bool use_normal_mapping;

uniform bool has_specular;


uniform sampler2D texture_diffuse;
uniform sampler2D texture_specular;
uniform sampler2D texture_normal;

vec3 CalculateNormal()
{
	// Gram-Schmidt, create orthogonal vectors
	vec3 normal = normalize(fragNormal);
	vec3 tangent = normalize(fragTangent);
	tangent = normalize(tangent - dot(tangent, normal) * normal);

	vec3 bitangent = cross(tangent, normal);

	// Get normal from normal texture
	vec3 displaced_normal = texture(texture_normal, fragTexCoord).rgb;
	displaced_normal = displaced_normal * 2.0 - 1.0;

	// Convert to tanget space
	vec3 new_normal;
	mat3 TBN = mat3(tangent, bitangent, normal);
	new_normal = TBN * displaced_normal;

	// Convert back to [0,1]
	//new_normal = new_normal * 0.5 + 0.5;
	new_normal = normalize(new_normal);

	return new_normal;
}

void main()
{
	gPosition = fragPosition;

	if(use_normal_mapping)
	{
		gNormal = CalculateNormal();
	}
	else
	{
		gNormal = normalize(fragNormal);	
	}

	if(use_textures)
	{
		gColorSpec.xyz = texture(texture_diffuse, fragTexCoord).rgb;

		if(has_specular)
		{
			gColorSpec.a = texture(texture_specular, fragTexCoord).r;
		}
		else
		{
			gColorSpec.a = 1.0;
		}
	}
	else
	{
		gColorSpec.xyz = fragColor;
		gColorSpec.a = 0.5;
	}


}