#ifndef MODEL_H
#define MODEL_H

#include <string>
#include <vector>

#include <GL/glew.h>
#include <glm/glm.hpp>

#include "Mesh.h"

//#define TINYOBJLOADER_IMPLEMENTATION
#include <tinyobjloader/tiny_obj_loader.h>

enum class RenderMode 
{
	TEXTURED,
	DIFFUSE
};

using namespace glm;

class Model
{
public:
	Model();
	Model(std::string model_path, std::string model_file, bool vertexOnly = false);

	void LoadModel();
	void LoadModelVertexOnly();

	void Render(Shader& shader);

	std::vector<Mesh> meshes;

	std::vector<Texture> textures;

private:
	//std::vector<Texture> LoadTexturesFromMaterial(tinyobj::material_t& material);

	void LoadTextures(const std::vector<tinyobj::material_t>& materials);

	vec3 GetDiffuseColor(const tinyobj::shape_t & shape, const std::vector<tinyobj::material_t>& materials);



	std::string directory;
	std::string file;
};

#endif // !MODEL_H

