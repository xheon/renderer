#include "Shader.h"

// GLEW
#include <GL/glew.h>

// GLFW
#include <GLFW/glfw3.h>

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <iostream>

using namespace glm;


Shader::Shader(const std::string & vertex_path, const std::string & fragment_path) :
	vertexShader_file(vertex_path), fragmentShader_file(fragment_path)
{
	// Read vertex shader file
	std::ifstream vs_file(vertex_path);
	std::string vs_src((std::istreambuf_iterator<char>(vs_file)), std::istreambuf_iterator<char>());

	// Read fragment shader file
	std::ifstream fs_file(fragment_path);
	std::string fs_src((std::istreambuf_iterator<char>(fs_file)), std::istreambuf_iterator<char>());

	// Create shader objects
	GLuint vertexShader = glCreateShader(GL_VERTEX_SHADER);
	GLuint fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);

	const char* vs = vs_src.c_str();
	const char* fs = fs_src.c_str();

	glShaderSource(vertexShader, 1, &vs, nullptr);
	glShaderSource(fragmentShader, 1, &fs, nullptr);

	// Compile vertex shader
	glCompileShader(vertexShader);

	GLint success;
	GLchar infoLog[512];
	glGetShaderiv(vertexShader, GL_COMPILE_STATUS, &success);
	if (!success)
	{
		glGetShaderInfoLog(vertexShader, 512, NULL, infoLog);
		std::cout << vertexShader_file;
		std::cout << "ERROR::SHADER::VERTEX::COMPILATION_FAILED\n" << infoLog << std::endl;
	}

	// Compile fragment shader
	glCompileShader(fragmentShader);

	glGetShaderiv(fragmentShader, GL_COMPILE_STATUS, &success);
	if (!success)
	{
		glGetShaderInfoLog(fragmentShader, 512, NULL, infoLog);
		std::cout << fragmentShader_file;
		std::cout << "ERROR::SHADER::FRAGMENT::COMPILATION_FAILED\n" << infoLog << std::endl;
	}

	// Create shader program
	program = glCreateProgram();

	glAttachShader(program, vertexShader);
	glAttachShader(program, fragmentShader);
	glLinkProgram(program);

	glGetProgramiv(program, GL_LINK_STATUS, &success);
	if (!success) {
		glGetProgramInfoLog(program, 512, NULL, infoLog);
		std::cout << "ERROR::SHADER::PROGRAM::LINKING_FAILED\n" << infoLog << std::endl;
	}

	glDeleteShader(vertexShader);
	glDeleteShader(fragmentShader);
}

void Shader::SetMat4(const std::string & uniform, mat4 matrix)
{
	auto location = GetUniformLocation(uniform);
	glUniformMatrix4fv(location, 1, GL_FALSE, value_ptr(matrix));
}

void Shader::SetVec3(const std::string & uniform, vec3 value)
{
	auto location = GetUniformLocation(uniform);
	glUniform3f(location, value.x, value.y, value.z);
}

void Shader::SetVec2(const std::string & uniform, vec2 value)
{
	auto location = GetUniformLocation(uniform);
	glUniform2f(location, value.x, value.y);
}

void Shader::SetFloat(const std::string & uniform, float value)
{
	auto location = GetUniformLocation(uniform);
	glUniform1f(location, value);
}

void Shader::SetInt(const std::string & uniform, int value)
{
	auto location = GetUniformLocation(uniform);
	glUniform1i(location, value);
}

void Shader::SetBool(const std::string & uniform, bool value)
{
	auto location = GetUniformLocation(uniform);
	glUniform1i(location, value);
}

void Shader::EnableAttribute(const std::string & attrib)
{
	auto loc = glGetAttribLocation(program, attrib.c_str());
	glEnableVertexAttribArray(loc);
}

void Shader::SetAttribute(const std::string & attrib, int size, GLenum type, bool normalized, int stride, void * pointer)
{
	auto loc = GetAttributeLocation(attrib);

	glVertexAttribPointer(loc, size, type, normalized, stride, pointer);
}

GLuint Shader::GetAttributeLocation(const std::string& attrib)
{
	auto loc = glGetAttribLocation(program, attrib.c_str());
	return loc;
}

GLuint Shader::GetUniformLocation(const std::string& uniform)
{
	GLuint location = glGetUniformLocation(program, uniform.c_str());

	return location;
}

//int Shader::AddAttribute(const std::string & attribute)
//{
//	int loc = glGetAttribLocation(program, attribute.c_str());
//
//	attributes.emplace(attribute, loc);
//
//	return loc;
//}

void Shader::Use()
{
	glUseProgram(program);
}


