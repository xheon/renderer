#version 330 core

in vec2 TexCoords;

uniform sampler2D filter_texture;

uniform vec2 texture_size = vec2(1280, 650);
vec2 inv_texture_size = 1.0 / texture_size;

const float span_max = 8.0;
const float reduce_min = 1.0 / 128.0;
const float reduce_mul = 1.0 / 8.0;

out vec4 fragColor; 

const vec3 luma = vec3(0.299, 0.587, 0.114);	

void main()
{
	// Sample 4 neighbors and mid
	// Convert to luminence (brightness)
	float luma_nw = dot(luma, texture2D(filter_texture, TexCoords.xy + vec2(-1.0, -1.0) * inv_texture_size).xyz);
	float luma_ne = dot(luma, texture2D(filter_texture, TexCoords.xy + vec2( 1.0, -1.0) * inv_texture_size).xyz);
	float luma_sw = dot(luma, texture2D(filter_texture, TexCoords.xy + vec2(-1.0,  1.0) * inv_texture_size).xyz);
	float luma_se = dot(luma, texture2D(filter_texture, TexCoords.xy + vec2( 1.0,  1.0) * inv_texture_size).xyz);
	float luma_mid  = dot(luma, texture2D(filter_texture, TexCoords.xy).xyz);

	vec2 direction;
	direction.x = -((luma_nw + luma_ne) - (luma_sw + luma_se)); // Horizontal
	direction.y =  ((luma_nw + luma_sw) - (luma_ne + luma_se)); // Vertical
	
	// Compute average of neighbors
	// Reduce value and clamp to upper value
	float direction_factor = max((luma_nw + luma_ne + luma_sw + luma_se) * (reduce_mul * 0.25), reduce_min);

	// Invert, take minimum direction
	float inv_dir_adj = 1.0/(min(abs(direction.x), abs(direction.y)) + direction_factor);
	
	// Maximum search radius in tex coords
	direction = min(vec2(span_max, span_max), 
		max(vec2(-span_max, -span_max), direction * inv_dir_adj)) * inv_texture_size;

	// Define short range value
	vec3 result_min = (1.0/2.0) * (
		texture2D(filter_texture, TexCoords.xy + (direction * vec2(1.0/3.0 - 0.5))).xyz +
		texture2D(filter_texture, TexCoords.xy + (direction * vec2(2.0/3.0 - 0.5))).xyz);

	// Define long range value
	vec3 result_max = result_min * (1.0/2.0) + (1.0/4.0) * (
		texture2D(filter_texture, TexCoords.xy + (direction * vec2(0.0/3.0 - 0.5))).xyz +
		texture2D(filter_texture, TexCoords.xy + (direction * vec2(3.0/3.0 - 0.5))).xyz);

	// Find minimum and maximum luminences
	float luma_min = min(luma_mid, min(min(luma_nw, luma_ne), min(luma_sw, luma_se)));
	float luma_max = max(luma_mid, max(max(luma_nw, luma_ne), max(luma_sw, luma_se)));
	float luma_result = dot(luma, result_max);
	
	if(luma_result < luma_min || luma_result > luma_max)
	{
		fragColor = vec4(result_min, 1.0);
	}
	else
	{
		fragColor = vec4(result_max, 1.0);
	}
}