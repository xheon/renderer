#include "Engine.h"

using namespace glm;

int main(int argc, char *args[])
{
	Engine engine;
	
	// Initialize engine
	engine.Init();

	// Game loop
	while (engine.IsRunning())
	{	
		engine.Update();
	}

	// Close GLFW
	glfwTerminate();

	return 0;
}