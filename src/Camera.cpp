#include "Camera.h"

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

Camera::Camera(vec3 position, vec3 up, float yaw, float pitch) :
	position(position), worldUp(up), yaw(yaw), pitch(pitch),
	front({ 0, 0, -1 }), speed(SPEED), sensitivity(SENSITIVITY)
{
	Update();
}

mat4 Camera::GetViewMatrix()
{
	return lookAt(position, position + front, up);
}

void Camera::Update()
{
	vec3 newFront;
	newFront.x = cos(radians(yaw)) * cos(radians(pitch));
	newFront.y = sin(radians(pitch));
	newFront.z = sin(radians(yaw)) * cos(radians(pitch));
	front = normalize(newFront);

	right = normalize(cross(front, worldUp));
	up = normalize(cross(right, front));
}

void Camera::ProcessKeyboard(Key direction, float delta)
{
	float velocity = speed * delta;

	
	if (direction == Key::FORWARD)
	{
		position += velocity * front;
	}

	if (direction == Key::BACKWARD)
	{
		position -= velocity * front;
	}

	if (direction == Key::LEFT)
	{
		position -= velocity * right;
	}

	if (direction == Key::RIGHT)
	{
		position += velocity * right;
	}
}

void Camera::ProcessMouse(float xoffset, float yoffset)
{
	xoffset *= sensitivity;
	yoffset *= sensitivity;

	yaw += xoffset;
	pitch += yoffset;

	if (pitch > 89.0f)
	{
		pitch = 89.0f;
	}

	if (pitch < -89.0f)
	{
		pitch = -89.0f;
	}

	Update();
}
