#pragma once

enum class Key
{
	FORWARD,
	BACKWARD,
	LEFT,
	RIGHT,
	LEFT_BUTTON
};