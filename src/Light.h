#ifndef LIGHT_H
#define LIGHT_H

#include <glm/glm.hpp>

#include "Shader.h"

using namespace glm;

class Light
{
public:
	Light();
	Light(vec3 ambient, vec3 diffuse, vec3 specular);
	
	vec3 ambient;
	vec3 diffuse;
	vec3 specular;

	virtual void Use(Shader& shader);
};

#endif // !LIGHT_H
