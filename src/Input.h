#ifndef INPUT_H
#define INPUT_H

#include <unordered_map>

//#include <GLFW/glfw3.h>

#include "Window.h"
#include "Key.h"
#include "Camera.h"
//#include "UI.h"

class Input
{

public:
	Input(Window* window);
	//Input(Window* window, bool has_ui = true);

	void SetCallbacks();
	void Key_callback(GLFWwindow * window, int key, int scancode, int action, int mode);
	void Cursor_callback(GLFWwindow * window, double xpos, double ypos);
	void Mouse_callback(GLFWwindow* window, int button, int action, int mode);

	void ToggleInput(GLFWwindow* window, int element, int action);

	void ProcessInput(float delta, Camera* camera);

private:
	Window* window;

	// UI
	//UI* ui;
	bool has_ui;
	
	// Input
	std::unordered_map<Key, bool> input;
	std::unordered_map<int, Key> mapping;

	// Mouse input
	GLfloat lastX; //= 400;
	GLfloat lastY; //= 300;
	float offsetX;
	float offsetY;
	bool firstMouse; //= true;

	bool showGBuffers;
};


#endif // !INPUT_H
