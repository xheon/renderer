#include "Scene.h"

#include <iostream>

Scene::Scene()
{
	camera = Camera(vec3(0.0f, 7.0f, 30.0f));
}

void Scene::Init()
{
	InitObjects();

	InitLights();
}

void Scene::InitObjects()
{
	const int num = 4;

	float spacing = 10;

	float offset = (spacing * num) / 2 - 5;

	const vec3 start(-offset, 0, -offset);

	Model* model = new Model("resources/nanosuit/", "nanosuit.obj");

	for (int i = 0; i < num; ++i)
	{
		for (int j = 0; j < num; ++j)
		{
			vec3 pos = start + vec3(i, 0, j) * spacing;
			GameObject obj(pos);
			obj.model = *model;
			objects.push_back(obj);
		}
	}


	// Add ground plane
	model = new Model("resources/brick/", "brick.obj");
	GameObject plane(*model, vec3(0));
	plane.size = vec3(50);
	objects.push_back(plane);

	// Stack some cubes
	model = new Model();
	GameObject cube(*model, vec3(5, 0.5f, 7));
	cube.size = vec3(3);
	objects.push_back(cube);

	cube = GameObject(*model, vec3(5.75f, 1.5f, 7));
	cube.size = vec3(3);
	objects.push_back(cube);

	cube = GameObject(*model, vec3(6.5f, 0.5f, 7));
	cube.size = vec3(3);
	objects.push_back(cube);

	/*Model* model = new Model("resources/sponza/", "sponza.obj");
	GameObject obj(*model, vec3(0));
	obj.size = vec3(0.1);
	objects.push_back(obj);*/

}

void Scene::InitLights()
{
	// Initialize direcitonal light
	//dirLight = DirectionalLight(vec3(0.2f), vec3(0.5f), vec3(1.0f), vec3(-0.2f, -1.0f, -0.3f));


	// Initialize point lights
	for (int i = 0; i < 512; ++i)
	{

		pointLights.emplace_back(vec3(0.2f), vec3(0.5f), vec3(1.0f), vec3(),
			1.0f, 0.09f, 0.032f, i);
	}

	Model* box_model = new Model("resources/cube/", "cube.obj", true);
	box_model->LoadModelVertexOnly();
	box_model->meshes[0].diffuse = vec3(1, 0, 0);

	srand(13);

	/*for (int i = 0; i < 20; i++)
	{
		for (int j = 0; j < 20; j++)
		{
			int index = i * 20 + j;
			auto& pl = pointLights[index];

			int offset = 100 / 2;
			pl.position = vec3(i * 10 - offset, 6, j * 10 - offset);
		}
	}*/

	for (auto& pl : pointLights)
	{
		GameObject light;
		light.model = *box_model;
		
		// Define position
		vec3 position;
		position.x = (rand() % 80) - 40.0f;
		position.z = (rand() % 80) - 40.0f;
		position.y = ((rand() % 100) / 100.0f) * 15 + 0.5f;
		//position.y = 1;

		// Copy position to actual light
		light.position = pl.position;
		pl.position = position;

		// Define color
		vec3 diffuse;
		diffuse.x = ((rand() % 100) / 200.0f) + 0.5f;
		diffuse.y = ((rand() % 100) / 200.0f) + 0.5f;
		diffuse.z = ((rand() % 100) / 200.0f) + 0.5f;

		pl.diffuse = diffuse;
		light.model.meshes[0].diffuse = diffuse;
		
		debugPointLights.push_back(light);
	}

	for (auto& light : debugPointLights)
	{


	}
}

std::vector<GameObject>& Scene::GetObjects()
{
	return objects;
}

std::vector<PointLight>& Scene::GetLights()
{
	return pointLights;
}

std::vector<GameObject>& Scene::GetLightObjects()
{
	return debugPointLights;
}

void Scene::Update(float delta)
{
	if (!update_scene || first_update)
	{
		first_update = false;
		return;
	}
	
	for (int i = 0; i < pointLights.size(); i++)
	{
		auto& pl = pointLights[i];
		pl.position.x += velocity * delta;

		if (pl.position.x > 50)
		{
			pl.position.x = -50;
		}

		debugPointLights.at(i).position = pointLights.at(i).position;
		
	}
}
