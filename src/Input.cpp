#include "Input.h"

#include "Camera.h"
#include <imgui/imgui.h>

Input::Input(Window* window) :
	window(window)
{	// Define input mapping
	mapping.insert({ GLFW_KEY_W, Key::FORWARD });
	mapping.insert({ GLFW_KEY_S, Key::BACKWARD });
	mapping.insert({ GLFW_KEY_A, Key::LEFT });
	mapping.insert({ GLFW_KEY_D, Key::RIGHT });
	mapping.insert({ GLFW_MOUSE_BUTTON_LEFT, Key::LEFT_BUTTON });

	// Init input
	input.insert({ Key::FORWARD, false });
	input.insert({ Key::BACKWARD, false });
	input.insert({ Key::LEFT, false });
	input.insert({ Key::RIGHT, false });
	input.insert({ Key::LEFT_BUTTON, false });

	lastX = 400;
	lastY = 300;
	firstMouse = true;
	showGBuffers = false;

	SetCallbacks();
}

void Input::SetCallbacks()
{
	// Put input object as user data for the callback functions
	glfwSetWindowUserPointer(window->glWindow, this);

	// Set keyboard input callback
	auto func1 = [](GLFWwindow* w, int key, int sc, int ac, int mode)
	{
		static_cast<Input*>(glfwGetWindowUserPointer(w))->Key_callback(w, key, sc, ac, mode);
	};

	glfwSetKeyCallback(window->glWindow, func1);

	// Define cursor input
	auto func2 = [](GLFWwindow* w, double x, double y)
	{
		static_cast<Input*>(glfwGetWindowUserPointer(w))->Cursor_callback(w, x, y);
	};

	glfwSetCursorPosCallback(window->glWindow, func2);

	// Define mouse button inputs
	auto func3 = [](GLFWwindow* w, int btn, int ac, int mode)
	{
		static_cast<Input*>(glfwGetWindowUserPointer(w))->Mouse_callback(w, btn, ac, mode);
	};

	glfwSetMouseButtonCallback(window->glWindow, func3);

	//glfwSetInputMode(window->glWindow, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
}

void Input::Key_callback(GLFWwindow * window, int key, int scancode, int action, int mode)
{
	Input* inp = static_cast<Input*>(glfwGetWindowUserPointer(window));

	if (action == GLFW_PRESS)
	{
		switch (key)
		{
		case GLFW_KEY_ESCAPE:
			glfwSetWindowShouldClose(window, GL_TRUE);
			break;

		case GLFW_KEY_F1:
			inp->showGBuffers = !inp->showGBuffers;
			break;

		default:
			break;
		}
	}

	inp->ToggleInput(window, key, action);

	// Add UI keys
	if (has_ui == true)
	{
		//inp->ui->SetKeys(key, action);
	}
}

void Input::Cursor_callback(GLFWwindow * window, double xpos, double ypos)
{
	Input* inp = static_cast<Input*>(glfwGetWindowUserPointer(window));
	
	if (ImGui::GetIO().WantCaptureMouse == false)
	{
		if (inp->firstMouse)
		{
			inp->lastX = xpos;
			inp->lastY = ypos;
			inp->firstMouse = false;
		}

		if (inp->input.at(Key::LEFT_BUTTON) == true)
		{
			inp->offsetX = xpos - inp->lastX;
			inp->offsetY = inp->lastY - ypos; // Reversed, y-coords go from bottom to top
			inp->lastX = xpos;
			inp->lastY = ypos;
		}
	}

}

void Input::Mouse_callback(GLFWwindow * window, int button, int action, int mode)
{
	Input* inp = static_cast<Input*>(glfwGetWindowUserPointer(window));

	inp->ToggleInput(window, button, action);

	if (action == GLFW_PRESS)
	{
		glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_HIDDEN);
		inp->firstMouse = true;
	}

	if (action == GLFW_RELEASE)
	{
		glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
	}
}

void Input::ToggleInput(GLFWwindow* window, int element, int action)
{
	Input* inp = static_cast<Input*>(glfwGetWindowUserPointer(window));
	
	if (action == GLFW_PRESS)
	{
		auto& m = inp->mapping.find(element);
		if (m != inp->mapping.end())
		{
			// Find key action
			Key k = m->second;
			inp->input.at(k) = true;
		}
	}

	if (action == GLFW_RELEASE)
	{
		auto& m = inp->mapping.find(element);
		if (m != inp->mapping.end())
		{
			// Find key action
			Key k = m->second;
			inp->input.at(k) = false;
		}
	}
}

void Input::ProcessInput(float delta, Camera* camera)
{
	camera->ProcessMouse(offsetX, offsetY);
	offsetX = 0;
	offsetY = 0;

	
	if (input.at(Key::FORWARD) == true)
	{
		camera->ProcessKeyboard(Key::FORWARD, delta);
	}

	if (input.at(Key::BACKWARD) == true)
	{
		camera->ProcessKeyboard(Key::BACKWARD, delta);
	}

	if (input.at(Key::LEFT) == true)
	{
		camera->ProcessKeyboard(Key::LEFT, delta);
	}

	if (input.at(Key::RIGHT) == true)
	{
		camera->ProcessKeyboard(Key::RIGHT, delta);
	}
}

