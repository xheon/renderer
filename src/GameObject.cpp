#include "GameObject.h"

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

GameObject::GameObject() :
	position(0.0f), size(1.0f)
{
}

GameObject::GameObject(vec3 position) :
	position(position), size(1.0f)
{
}

GameObject::GameObject(Model model, vec3 position) :
	model(model), position(position), size(1.0f)
{
}

void GameObject::Render(Shader & shader, mat4 view_matrix, mat4 proj_matrix)
{
	// Create transformation matrix
	mat4 scale_matrix = scale(mat4(), size);
	mat4 rotation_matrix; // = rotate(mat4(), rotation); // TODO: Add object rotation
	mat4 translation_matrix = translate(mat4(), position);
	
	mat4 model_matrix = scale_matrix * rotation_matrix * translation_matrix;

	shader.SetMat4("model", model_matrix);
	//shader.SetMat4("view", view_matrix);
	//shader.SetMat4("proj", proj_matrix);

	model.Render(shader);
}

void GameObject::Render(Shader & shader)
{
	// Create transformation matrix
	mat4 scale_matrix = scale(mat4(), size);
	mat4 rotation_matrix; // = rotate(mat4(), rotation); // TODO: Add object rotation
	mat4 translation_matrix = translate(mat4(), position);

	mat4 model_matrix = scale_matrix * rotation_matrix * translation_matrix;

	shader.SetMat4("model", model_matrix);

	model.Render(shader);
}
