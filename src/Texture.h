#ifndef TEXTURE_H
#define TEXTURE_H

#include <string>

#include <GL/glew.h>

#include <glm/glm.hpp>

using namespace glm;

enum class TextureType
{
	DIFFUSE,
	NORMAL,
	SPECULAR
};

class Texture
{
public:
	Texture(std::string& path, TextureType type);
	Texture(vec3 color, TextureType type = TextureType::DIFFUSE);

	
	int height;
	int width;
	GLuint texture;
	std::string filename;
	TextureType type;

private:
	void LoadTexture();
	std::string NormalizePath(std::string & path);
};

#endif // !TEXTURE_H

