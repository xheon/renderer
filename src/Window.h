#ifndef WINDOW_H
#define WINDOW_H

#include <string>

#include <GLFW\glfw3.h>

class Window
{
public:
	Window();
	Window(int width, int height);
	Window(int width, int height, int x, int y);
	Window(int width, int height, int x, int y, std::string title);

	void Init();

	GLFWwindow* glWindow;

	float GetAspectRatio();

	int width;
	int height;

	int x;
	int y;

	std::string title;
};

#endif // !WINDOW_H