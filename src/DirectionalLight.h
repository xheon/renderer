#ifndef DIRECTIONALLIGHT_H
#define DIRECTIONALLIGHT_H

#include "Light.h"

class DirectionalLight : public Light
{
public:
	DirectionalLight();
	DirectionalLight(vec3 ambient, vec3 diffuse, vec3 specular, vec3 direction);
	
	vec3 direction;

	void Use(Shader& shader) override;
};

#endif // !DIRECTIONALLIGHT_H
