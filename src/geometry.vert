#version 330 core

layout (location = 0) in vec3 position;
layout (location = 1) in vec3 normal;
layout (location = 2) in vec2 texCoord;
layout (location = 3) in vec3 tangent;

uniform mat4 model;
uniform mat4 view;
uniform mat4 proj;


out vec3 fragPosition;
out vec3 fragNormal;
out vec2 fragTexCoord;
out vec3 fragColor;
out vec3 fragTangent;

void main()
{
	vec4 viewPos = view * model * vec4(position, 1.0);
	fragPosition = viewPos.xyz;
	fragTexCoord = texCoord;

	mat3 normal_mat = transpose(inverse(mat3(view * model)));
	fragNormal = normal_mat * normal;

	fragTangent = normal_mat * tangent;

	fragColor = vec3(0.95);

	gl_Position = proj * viewPos;

}