#include "PointLight.h"

#include <string>

PointLight::PointLight(vec3 ambient, vec3 diffuse, vec3 specular, vec3 position, 
	float constant, float linear, float quadratic, int index) :
	Light(ambient, diffuse, specular), position(position),
	constant(constant), linear(linear), quadratic(quadratic), index(index)
{
}

void PointLight::Use(Shader & shader)
{
	std::string uniform_name = "pointLights[" + std::to_string(index) + "]";

	/*shader.SetVec3(uniform_name + ".ambient", ambient);
	shader.SetVec3(uniform_name + ".diffuse", diffuse);
	shader.SetVec3(uniform_name + ".specular", specular);*/
	shader.SetVec3(uniform_name + ".color", diffuse);

	shader.SetVec3(uniform_name + ".position", position);

	shader.SetFloat(uniform_name + ".constant",	 constant);
	shader.SetFloat(uniform_name + ".linear",	 linear);
	shader.SetFloat(uniform_name + ".quadratic", quadratic);
}

void PointLight::Use(Shader & shader, const mat4& view)
{
	std::string uniform_name = "pointLights[" + std::to_string(index) + "]";

	/*shader.SetVec3(uniform_name + ".ambient", ambient);
	shader.SetVec3(uniform_name + ".diffuse", diffuse);
	shader.SetVec3(uniform_name + ".specular", specular);*/
	shader.SetVec3(uniform_name + ".color", diffuse);

	vec3 pos = vec3(view * vec4(position, 1.0f));
	shader.SetVec3(uniform_name + ".position", pos);

	shader.SetFloat(uniform_name + ".constant", constant);
	shader.SetFloat(uniform_name + ".linear", linear);
	shader.SetFloat(uniform_name + ".quadratic", quadratic);
}

